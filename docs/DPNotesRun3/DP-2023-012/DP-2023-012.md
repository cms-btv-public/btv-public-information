# A first look at early 2022 proton-proton collisions at √s = 13.6 TeV for heavy-flavor jet tagging

**CDS link:** [DP-2023-012](https://cds.cern.ch/record/2854698?ln=en)

# Abstract

Identification of jets stemming from heavy-flavor (bottom or charm) hadrons relies primarily on the inputs due to reconstruction of charged particle tracks and secondary vertices contained in these jets. Thus, it is crucial to study the data vs. simulation distributions of the kinematic variables that are used as inputs for the heavy-flavor tagging algorithms. With the start of LHC Run 3, new track reconstruction methods and changes of the general data-taking conditions have been introduced in the CMS experiment. In this note, a comparison of early Run 3 data vs. simulation distributions for several input variables, tagging discriminants, and other relevant kinematic observables in four different phase space regions that are enriched in b, c, and light (udsg) jets, is presented. The proton-proton collision data recorded by the CMS detector during the early part of the 2022 run, corresponding to an integrated luminosity of 7.65 fb⁻¹, has been used for this study.

# Glossary

- **AK4 jets:** Jets are reconstructed from particle-flow candidates using the anti-$k_T$ algorithm [1] with a distance parameter of
R = 0.4. The pileup mitigation is performed with the pileup-per-particle identification (PUPPI) algorithm [2], which assigns a weight to every particle depending on its probability to originate from a pileup vertex or the primary vertex. Dedicated jet energy corrections (JEC) derived based on early Run 3 data [3] are applied to the jets.

- **Muon-jet:** A type of jet that contains a low-$p_T$ muon, i.e. fulfilling the requirement on the angular separation of ΔR(low-$p_T$ muon, jet axis) < 0.4.

- **Pileup jet:** Reconstructed jets that are not mapped to any generator-level jet with $p_T$ > 8 GeV in the simulated event, are attributed to be originating from pileup vertices. These jets are marked as pileup jets. 

- **DeepJet:** A multi-classification deep-neural-network algorithm [4] employing general (low-level) properties of several charged and neutral particle-flow jet constituents, supplemented also with properties of secondary vertices associated with a jet. For each collection of charged and neutral particles and vertices, separate 1x1 convolutional layers are trained with different levels of filters acting on each particle (charged and neutral) or vertex individually. The output of the convolutional layers is further propagated to a collection of recurrent layers (LSTMs). The outputs of the LSTMs are merged with global jet properties and propagated through one dense layer with 200 nodes, followed by 7 hidden dense layers with 100 nodes each. It has 6 output nodes, namely, b, bb, leptonic b, c, uds and g, assigning a probability for each jet to belong to any of these categories.

- **CvB and CvL discriminators:** As charm (c) jets have properties intermediate to that of bottom (b) and light-flavor (udsg) jets, one needs to define two discriminators, each of which discriminates c jets against udsg or b jets. This is done by defining CvL and CvB values for each jet using the outputs from DeepJet tagger, as CvL = P(c) / [P(c) + P(udsg)] and CvB = P(c) / [P(c) + P(b)]. P(b) is defined as the sum of the b, bb, and leptonic b output nodes, and P(udsg) is given by the sum of the uds and g output scores.

- **Event topologies:**
    - **Dileptonic top quark antiquark (tt̅) phase space:** Dileptonically decaying tt̅ events form the final state in which the highest purity of b jets is achieved. This event topology is relevant for deriving calibration scale factors for b-tagging [5]. Events are selected with a set of electron-muon (eμ) dilepton trigger paths. The electron (muon) is required to fulfill $p_T$ > 30 (30) GeV, |η| < 2.5 (2.4) and to pass tight identification and isolation requirements [7,8]. At least 2 jets with $p_T$ > 20 GeV, |η| < 2.5, fulfilling tight identification criteria, and cleaned from the selected electron and muon are considered.
    - **Semileptonic tt̅ phase space:** Due to the hadronically decaying W boson, semileptonic tt̅ events have a significant amount of c jets, and can be thus used for calculating c-tagging and b-tagging identification SFs [5,6]. Events are selected using a single-muon trigger path, the selected muon is required to fulfill the same selection criteria as the one of the dileptonic tt̅ phase space given above. At least 4 jets with the same requirements as above are considered. The event is required to have $p_T^{miss}$ > 50 GeV.
    - **W boson plus charm jet (W+c) phase space:** This phase space is largely enriched in c jets and is employed for evaluating the c-tagging performance of the heavy-flavor tagging algorithms [5,6]. The same trigger path and the same selection criteria for the isolated muon as for the semileptonic tt̅ phase space are required. At least 1 additional soft muon with a reduced $p_T$ threshold and a relative isolation of >0.2 is selected and matched with 1–3 selected muon-jets, which pass $p_T$ > 20 GeV and |η| < 2.5. Both opposite-sign (OS) and same-sign (SS) isolated muon and soft muon pairs are taken into account. Events that contain more than 1 reconstructed secondary vertex are considered for this study. Additional selection criteria to enrich W boson events and to suppress QCD multijet and Drell–Yan contributions are applied as well. For the enrichment in W bosons, an invariant mass of the sum of $p_T^{miss}$ and the isolated muon four-vector of >55 GeV is required. Events stemming from Drell–Yan processes are suppressed by excluding an invariant di-muon mass within the Z boson mass window (80 GeV < $m_{\mu\mu}$ < 100 GeV). By requiring an upper threshold of 0.4 for the muon energy fraction in the muon jet, and by requiring the sum of the muon and the neutral electromagnetic energy fractions to be smaller than 0.4, Drell–Yan events are further suppressed. Low-mass di-muon events are rejected by selecting events with $m_\mu\mu$ > 12 GeV. The QCD multijet rejection is performed by requiring the isolated muon to fulfill a very tight relative isolation of <0.05 and tight requirements on the impact parameter of the selected muon.
    - **Drell–Yan plus jets (DY+jets) phase space:** This phase space is enriched in udsg jets and is used for the calibration of udsg jets [5,6]. A di-muon trigger path is employed to selected Z→μμ events. The leading (subleading) muon fulfill $p_T$ > 15 (12) GeV, boths muons are required to satisfy |η| < 2.4, as well as tight identification and isolation requirements [8]. The invariant di-muon mass has to be at least 15 GeV and to be within the Z boson mass window. At least 1 jet with $p_T$ > 20 GeV, |η| < 2.5, fulfilling tight identification criteria, and cleaned from the selected muons is required.

# Run 3 tracking conditions

**Beam spot position:** Early Run 3 simulation has been simulated with the pixel detector position at (0,0,0) and the longitudinal beamspot position is misplaced during collisions. In early Run 3 data, the pixel detector position and the centroid of the luminous region are free-floating parameters, resulting in a disagreement between data and simulation for the longitudinal beamspot position [9]. Several kinematic variables, e.g. the flight distance significance of the secondary vertex with respect to the primary vertex, are sensitive to this issue.

**Innermost barrel pixel layer of CMS tracker:** During the long shutdown period between 2019 and 2021, the innermost barrel pixel layer of the CMS tracker has been replaced with new modules. At the very beginning of Run 3 collisions during 2022, agreement between data and simulation for the signed impact parameter significance of the tracks with respect to the primary vertex was found to have worsened with time. This effect was attributed to the ageing of the innermost barrel pixel layer caused by accumulated irradiation during very first collisions. The agreement was improved by updating the high-voltages and the alignment during the later period of 2022 collisions [9] . Several kinematic input variables related to secondary vertices and charged particle tracks shown in this note are sensitive to this issue.

# Dileptonic tt̅ phase space

!!! danger "**Figure 1**"
    [![](plots/ttdilep/tt_emu.png){ width=990 }](plots/ttdilep/tt_emu.png)

    
    **Figure 1.** Candidate event recorded in 2022 during Run 3 proton-proton (pp) collisions by the CMS detector [10]. The event contains a negatively charged muon (red line) and a positively charged electron (green line) with $p_T$ = 42.6 and 76.1 GeV respectively, along with two jets (in orange cones) with $p_T$ above 150 GeV. The event also has a $p_T^{miss}$ (magenta arrow) of 87.9 GeV. This event passes the selection criteria applied for the dileptonic tt̅ phase space (cf. glossary). Each jet usually contains several charged particle tracks within its cone that are not included in the figure for the purposes of clarity. The ECAL and Muon barrel detectors are shown in the figure in light blue and grey-red cones respectively. The figure is made using the “iSPY WebGL” software tool [11].


!!! danger "**Figure 2**"
    [![](plots/ttdilep/unc_jet0_pt_inclusive_all.png){ width=495 }](plots/ttdilep/unc_jet0_pt_inclusive_all.png)
    [![](plots/ttdilep/unc_jet0_eta_inclusive_all.png){ width=495 }](plots/ttdilep/unc_jet0_eta_inclusive_all.png))

    **Figure 2:** Transverse momentum ($p_T$, left) and pseudorapidity (η, right) of the selected jet with highest $p_T$. This phase space is enriched in b jets. Reasonable agreement for the pT and η distributions is observed.


!!! danger "**Figure 3**"
    [![](plots/ttdilep/unc_btagDeepFlavB_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_btagDeepFlavB_0_inclusive_log.png)
    [![](plots/ttdilep/unc_btagDeepFlavC_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_btagDeepFlavC_0_inclusive_log.png)

    **Figure 3.** DeepJet b-tagging (left) and c-tagging discriminants (right) of the first two selected jets with highest $p_T$. As the DeepJet tagger used for this study was trained with Run 2 simulation and as no b-tagging calibration scale factors have been applied to these distributions, the observed disagreement between Run 3 data and simulation is expected.


!!! danger "**Figure 4**"
    [![](plots/ttdilep/unc_DeepJet_sv_dxy_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_DeepJet_sv_dxy_0_inclusive_log.png)
    [![](plots/ttdilep/unc_DeepJet_sv_dxysig_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_DeepJet_sv_dxysig_0_inclusive_log.png)

    **Figure 4.** Value (left) and significance (right) of the 2D flight distance of the first selected secondary vertex relative to the primary vertex. Reasonable agreement is observed for the value, while disagreement is visible for the significance. The disagreement is related to the issues in early Run 3 tracking detector conditions mentioned in page 6.


!!! danger "**Figure 5**"
    [![](plots/ttdilep/unc_DeepJet_sv_pt_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_DeepJet_sv_pt_0_inclusive_log.png)
    [![](plots/ttdilep/unc_DeepJet_sv_chi2_0_inclusive_log.png){ width=495 }](plots/ttdilep/unc_DeepJet_sv_chi2_0_inclusive_log.png)

    **Figure 5.** Transverse momentum ($p_T$, left) and $\chi^2$ value of the secondary vertex fit (right) of the first selected secondary vertex. Good agreement is observed for the $p_T$ distribution, a small discrepancy between data and simulation is observed at high $\chi^2$ values.

# Semileptonic tt̅ phase space

!!! danger "**Figure 6**"
    [![](plots/ttsemilep/tt_semilep_mu.png){ width=990 }](plots/ttsemilep/tt_semilep_mu.png)

    
    **Figure 6.** Candidate event recorded in 2022 during Run 3 proton-proton (pp) collisions by the CMS detector [10]. The event contains a negatively charged muon (red line) with $p_T$ = 34 GeV and four jets (orange cones) with $p_T$ > 60 GeV. The event also has a $p_T^{miss}$ (magenta arrow) of 85.6 GeV. This event passes the selection criteria applied for the semileptonic tt̅ phase space (cf. glossary). Each jet usually contains several charged particle tracks within its cone that are not included in the figure for the purposes of clarity. The ECAL barrel and Muon barrel detectors are shown in the figure in light blue and grey-red cones respectively. The figure is made using the “iSpy WebGL” software tool [11].


!!! danger "**Figure 7**"
    [![](plots/ttsemilep/unc_jet0_pt_inclusive_all.png){ width=495 }](plots/ttsemilep/unc_jet0_pt_inclusive_all.png)
    [![](plots/ttsemilep/unc_jet0_eta_inclusive_all.png){ width=495 }](plots/ttsemilep/unc_jet0_eta_inclusive_all.png)

    **Figure 7.** Transverse momentum ($p_T$, left) and pseudorapidity (η, right) of the selected jet with highest $p_T$. Compared to the dileptonic tt̅ final state (see Fig. 2), the fraction of udsg and c jets is higher. Horizontal error bars for the data points indicate the variation of the bin width across the x-axis. A slight disagreement for $p_T$ is observed, which is likely caused due to the QCD multijet contribution not being considered. Good agreement for the η distribution is observed.


!!! danger "**Figure 8**"
    [![](plots/ttsemilep/unc_btagDeepFlavB_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_btagDeepFlavB_0_inclusive_log.png)
    [![](plots/ttsemilep/unc_btagDeepFlavC_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_btagDeepFlavC_0_inclusive_log.png)

    **Figure 8.** DeepJet b-tagging (left) and c-tagging discriminants (right) of the first four selected jets with highest $p_T$. As the DeepJet tagger used for this study was trained with Run 2 simulation and as no b-tagging calibration scale factors have been applied to these distributions, the observed disagreement between Run 3 data and simulation is expected.


!!! danger "**Figure 9**"
    [![](plots/ttsemilep/unc_DeepJet_sv_d3d_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_DeepJet_sv_d3d_0_inclusive_log.png)
    [![](plots/ttsemilep/unc_DeepJet_sv_d3dsig_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_DeepJet_sv_d3dsig_0_inclusive_log.png)

    **Figure 9.** Value (left) and significance (right) of the 3D flight distance of the first selected secondary vertex relative to the primary vertex. Reasonable agreement is observed for the value, while disagreement is visible for the significance. The disagreement is related to the issues in early Run 3 tracking detector conditions mentioned in page 6.


!!! danger "**Figure 10**"
    [![](plots/ttsemilep/unc_DeepJet_Cpfcan_BtagPf_trackSip2dVal_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_DeepJet_Cpfcan_BtagPf_trackSip2dVal_0_inclusive_log.png)
    [![](plots/ttsemilep/unc_DeepJet_Cpfcan_BtagPf_trackSip2dSig_0_inclusive_log.png){ width=495 }](plots/ttsemilep/unc_DeepJet_Cpfcan_BtagPf_trackSip2dSig_0_inclusive_log.png)

    **Figure 10.** Value (left) and significance (right) of the 2D signed impact parameter (SIP) of the track of the first selected charged particle. Similarly to the 3D flight distance of the secondary vertex shown in Fig. 8, the observed disagreement is related to the issues in early Run 3 tracking detector conditions mentioned in page 6.

# W+c phase space

!!! danger "**Figure 11**"
    [![](plots/Wc/Wc_mu.png){ width=990 }](plots/Wc/Wc_mu.png)

    
    **Figure 11.** Candidate event recorded in 2022 during Run 3 proton-proton (pp) collisions by the CMS detector [10]. The event contains positively charged muon (in red curved line) with $p_T$ = 43.6 GeV and two jets (orange cones) with $p_T$ above 40 GeV. The event also has a $p_T^{miss}$ (magenta arrow) of 23.8 GeV. This event passes the selection criteria applied for the W+c phase space (cf. glossary). Each jet usually contains several charged particle tracks within its cone that are not included in the figure for the purposes of clarity. The ECAL barrel as well as Muon barrel and endcap detectors are shown in the figure in light blue and grey-red cones respectively. The figure is made using the “iSpy WebGL” software tool [11].


!!! danger "**Figure 12**"
    [![](plots/Wc/unc_mujet_pt_inclusive_all.png){ width=495 }](plots/Wc/unc_mujet_pt_inclusive_all.png)
    [![](plots/Wc/unc_mujet_eta_inclusive_all.png){ width=495 }](plots/Wc/unc_mujet_eta_inclusive_all.png)

    **Figure 12.** Transverse momentum ($p_T$, left) and pseudorapidity (η, right) of the selected muon-jet with highest $p_T$. The phase space consists mostly of c jets. Good agreement between data and simulation is observed.


!!! danger "**Figure 13**"
    [![](plots/Wc/unc_btagDeepFlavB_0_inclusive_log.png){ width=495 }](plots/Wc/unc_btagDeepFlavB_0_inclusive_log.png)
    [![](plots/Wc/unc_btagDeepFlavC_0_inclusive_log.png){ width=495 }](plots/Wc/unc_btagDeepFlavC_0_inclusive_log.png)

    **Figure 13.** DeepJet b-tagging (left) and c-tagging discriminants (right) of the selected jet with highest $p_T$. As the DeepJet tagger used for this study was trained with Run 2 simulation and as no b-tagging calibration scale factors have been applied to these distributions, the observed disagreement between Run 3 data and simulation is expected. Compared to the tt̅ phase space (see Figs. 3 and 8), data and simulation show better agreement.


!!! danger "**Figure 14**"
    [![](plots/Wc/unc_btagDeepFlavCvB_0_inclusive_log.png){ width=495 }](plots/Wc/unc_btagDeepFlavCvB_0_inclusive_log.png)
    [![](plots/Wc/unc_btagDeepFlavCvL_0_inclusive_log.png){ width=495 }](plots/Wc/unc_btagDeepFlavCvL_0_inclusive_log.png)

    **Figure 14.** DeepJet CvB (left) and CvL discriminants (right) of the selected jet with highest $p_T$. As the DeepJet version used for this study was trained with Run 2 simulation and as no b-tagging calibration scale factors have been applied to these distributions, the observed disagreement between Run 3 data and simulation is expected.


!!! danger "**Figure 15**"
    [![](plots/Wc/unc_DeepJet_sv_d3d_0_inclusive_log.png){ width=495 }](plots/Wc/unc_DeepJet_sv_d3d_0_inclusive_log.png)
    [![](plots/Wc/unc_DeepJet_sv_d3dsig_0_inclusive_log.png){ width=495 }](plots/Wc/unc_DeepJet_sv_d3dsig_0_inclusive_log.png)

    **Figure 15.** Value (left) and significance (right) of the 3D flight distance of the first selected secondary vertex relative to the primary vertex. Reasonable agreement is observed.


!!! danger "**Figure 16**"
    [![](plots/Wc/unc_DeepJet_Cpfcan_BtagPf_trackSip3dVal_0_inclusive_log.png){ width=495 }](plots/Wc/unc_DeepJet_Cpfcan_BtagPf_trackSip3dVal_0_inclusive_log.png)
    [![](plots/Wc/unc_DeepJet_Cpfcan_BtagPf_trackSip3dSig_0_inclusive_log.png){ width=495 }](plots/Wc/unc_DeepJet_Cpfcan_BtagPf_trackSip3dSig_0_inclusive_log.png)

    **Figure 16.** Value (left) and significance (right) of the 3D signed impact parameter (SIP) of the track of the first selected charged particle. Reasonable agreement is observed.

# DY+jets phase space

!!! danger "**Figure 17**"
    [![](plots/DY/DY_mumu.png){ width=990 }](plots/DY/DY_mumu.png)

    
    **Figure 17.** Candidate recorded in 2022 during Run 3 proton-proton (pp) collisions by the CMS detector [10]. The event contains two oppositely charged muons (red lines) with $p_T$ above 12 GeV along with two jets (orange cones) with $p_T$ above 35 GeV. The event also has a $p_T^{miss}$ (magenta arrow) of 62.5 GeV. This event passes the selection criteria applied for the DY+jets phase space (cf. glossary). Each jet usually contains several charged particle tracks within its cone that are not included in the figure for the purposes of clarity. The ECAL barrel and Muon endcap detectors are shown in the figure in light blue and grey-red cones respectively. The figure is made using the “iSpy WebGL” software tool [11].


!!! danger "**Figure 18**"
    [![](plots/DY/unc_jet_pt_inclusive_all.png){ width=495 }](plots/DY/unc_jet_pt_inclusive_all.png)
    [![](plots/DY/unc_jet_eta_inclusive_all.png){ width=495 }](plots/DY/unc_jet_eta_inclusive_all.png)

    **Figure 18.** Transverse momentum ($p_T$, left) and pseudorapidity (η, right) of the selected jet with highest $p_T$. The phase space consists mostly of udsg jets. Good agreement is observed for both distributions.


!!! danger "**Figure 19**"
    [![](plots/DY/unc_z_pt_inclusive_all.png){ width=495 }](plots/DY/unc_z_pt_inclusive_all.png)
    [![](plots/DY/unc_z_mass_inclusive_all.png){ width=495 }](plots/DY/unc_z_mass_inclusive_all.png)

    **Figure 19.** Comparison of the modeling of the Z boson $p_T$ (left) and Z boson mass (right) between simulation and data. The differences between simulation and data for these distributions are likely due to muon identification and momentum scale corrections being unavailable at the time of this study.


!!! danger "**Figure 20**"
    [![](plots/DY/unc_btagDeepFlavB_0_inclusive_log.png){ width=495 }](plots/DY/unc_btagDeepFlavB_0_inclusive_log.png)
    [![](plots/DY/unc_btagDeepFlavC_0_inclusive_log.png){ width=495 }](plots/DY/unc_btagDeepFlavC_0_inclusive_log.png))

    **Figure 20.** DeepJet b-tagging (left) and c-tagging discriminants (right) of the selected jet with highest $p_T$. As the DeepJet tagger used for this study was trained with Run 2 simulation and as no b-tagging calibration scale factors have been applied to these distributions, the observed disagreement between Run 3 data and simulation is expected.


!!! danger "**Figure 21**"
    [![](plots/DY/unc_DeepJet_sv_dxy_0_inclusive_log.png){ width=495 }](plots/DY/unc_DeepJet_sv_dxy_0_inclusive_log.png)
    [![](plots/DY/unc_DeepJet_sv_dxysig_0_inclusive_log.png){ width=495 }](plots/DY/unc_DeepJet_sv_dxysig_0_inclusive_log.png)

    **Figure 21.** Value (left) and significance (right) of the 2D flight distance of the first selected secondary vertex relative to the primary vertex. Reasonable agreement is observed.


!!! danger "**Figure 22**"
    [![](plots/DY/unc_DeepJet_Cpfcan_BtagPf_trackSip3dVal_0_inclusive_log.png){ width=495 }](plots/DY/unc_DeepJet_Cpfcan_BtagPf_trackSip3dVal_0_inclusive_log.png)
    [![](plots/DY/unc_DeepJet_Cpfcan_BtagPf_trackSip3dSig_0_inclusive_log.png){ width=495 }](plots/DY/unc_DeepJet_Cpfcan_BtagPf_trackSip3dSig_0_inclusive_log.png)

    **Figure 22.** Value (left) and significance (right) of the 3D signed impact parameter (SIP) of the track of the first selected charged particle. While the value is well modelled, disagreement is observed for the significance. The disagreement is related to the issues in early Run 3 tracking detector conditions mentioned in page 6.

# DeepJet Performance: Run 3 vs. Run 2 training

!!! danger "**Figure 23**"
    [![](plots/ROC_DP_note_ttbar.png){ width=990 }](plots/ROC_DP_note_ttbar.png)

    
    **Figure 23.** Comparison of the state-of-the-art DeepJet algorithm performances between the first training model derived using Run 3 simulation (labeled as “Run 3 Puppi”; red curves) and the one from Run 2 (labeled as “Run 2 CMSSW”; black curves). The probability of the misidentification of non-b jets as b jets relative to the efficiency  of identifying b jets is shown. Both training models have been evaluated on AK4 PUPPI jets with $p_T$ > 30 GeV and |η| < 2.5 using simulated top quark pair events. The results are shown separately for the misidentification of udsg jets (solid curves) and of c jets (dashed curves). We can observe an improvement of the DeepJet algorithm performance with the Run 3 training model compared to the Run 2 model. The performance improvement is more substantial for identifying b jets against udsg jets than against c jets.


# Summary

In this note, a first comparison of several observables between early Run 3 data collected during 2022 and simulation has been presented. Various kinematic variables as well as the input variables of the DeepJet tagging algorithm that are related to the properties of the reconstructed tracks and secondary vertices have been studied. In addition, the b- and c-tagging discriminants of the DeepJet algorithm are evaluated for jets in Run 3 data and simulation using the training model from Run 2. Observed data-to-simulation discrepancies for the DeepJet discriminants are expected to be decreased by a dedicated retraining of the algorithm for Run 3. A first training of the DeepJet algorithm with Run 3 simulation shows substantial performance improvement compared to that obtained using Run 2 training model. Improvement in the modeling of track and SV-based input variables is expected by a reprocessing of the Run 3 data and simulation where the effects due to ageing of the innermost barrel pixel layer of the CMS tracker as well as the correct beamspot position will be taken into account.

# References

[1] M. Cacciari, G. P. Salam and G. Soyez, “The anti-kt jet clustering algorithm,” JHEP 0804 (2008) 063.

[2] CMS Collaboration, “Pileup mitigation at CMS in 13 TeV data”, JINST 15 (2020) P09018.

[3] CMS Collaboration, “Jet Energy Scale and Resolution Measurements Using Prompt Run3 Data Collected by CMS in the First Months of 2022 at 13.6 TeV”, CMS Detector Performance Summary CMS-DP-2022-054.

[4] E. Bols, J. Kieseler, M. Verzetti, M. Stoye and A. Stakia, “Jet flavour classification using DeepJet” JINST 15 (2020) P12012.

[5] CMS Collaboration, “Identification of heavy-flavour jets with the CMS detector in pp collisions at 13 TeV”, JINST 13 (2018), P05011.

[6] CMS Collaboration, “A new calibration method for charm jet identification validated with proton-proton collision events at √s = 13 TeV”, JINST 17 (2022) P03014.

[7] CMS Collaboration, “Electron and photon reconstruction and identification with the CMS experiment at the CERN LHC", JINST 16 (2021) P05014.

[8] CMS Collaboration, “Performance of the CMS muon detector and muon reconstruction with proton-proton collisions at √s = 13 TeV”, JINST 13 (2018) P06015.

[9] CMS Collaboration, “Early Run 3 tracking performance”, CMS Detector Performance Summary CMS-DP-2022-064.

[10] CMS Collaboration, “The CMS Experiment at the CERN LHC”, JINST 3 (2008) S08004.

[11] T. McCauley, “A browser-based event display for the CMS Experiment at the LHC using WebGL” , J. Phys.: Conf. Ser. 898 072030.
