# CMS b-tagging and vertexing (BTV) results

**Note:** This page includes the public result originated from the old decommissioned [BTV Physics Results TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsBTV)

## Standard BTV publications

| **Title** | **CADI Entry (restricted)** | **Link** | **Reference** | **Remark** |
| --- | --- | --- | --- | --- |
| Performance of heavy-flavour jet identification in boosted topologies in proton-proton collisions at 13 TeV | BTV-22-001 | [CDS](https://cds.cern.ch/record/2866276) |  | 13 TeV, 2016-2018 |
| A new calibration method for charm jet identification validated with proton-proton collision events at 13 TeV | BTV-20-001 | [arXiv](https://arxiv.org/abs/2111.03027) | JINST 17 (2022) P03014 | 13 TeV, 2017 |
| Identification of heavy-flavour jets with the CMS detector in pp collisions at 13 TeV | BTV-16-002 | [arXiv](https://arxiv.org/abs/1712.07158) | JINST 13 (2018) P05011 | 13 TeV, 2016 |
| Identification of b-quark jets with the CMS experiment | BTV-12-001 | [arXiv](https://arxiv.org/abs/1211.4462) | JINST 8 (2013) P04013 | 8 TeV |

## DeepJet algorithm description


| **Title** | **CADI Entry (restricted)** | **Link** | **Reference** | **Remark** |
| --- | --- | --- | --- | --- |
| Jet Flavour Classification Using DeepJet | BTV-20-002 | [arXiv](https://arxiv.org/abs/2008.10519) | JINST 15 (2020) P12012 | 13 TeV Phase-I detector, Simulation only |

## Detector Performance Notes at Run 3

| **Topic** | **DP number** | **CDS entry** |**Wiki page** | **Approved plots** | **Remarks** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Early Run 3 commissioning | **DP-2023-012** | [CDS](https://cds.cern.ch/record/2854698) | [wiki](DPNotesRun3/DP-2023-012/DP-2023-012.md) | [plots](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun3/DP-2023-012/plots) | Run 3 2022 (7.65fb⁻¹) |
| Performance of DeepJet algorithm at HLT using Run 3 data | **DP-2023-018** | [CDS](https://cds.cern.ch/record/2856240) | [wiki](DPNotesRun3/DP-2023-018/DP-2023-018.md) | | Run 3 2022 (35.1fb⁻¹) |  
| Efficiency of the ParticleNet b-tagging algorithm used in the CMS High-Level Trigger in 2022 and 2023 | **DP-2023-089** | [CDS](https://cds.cern.ch/record/2881092) | [twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/Run3PNetBtag) | | Run 3 2022 (35.1fb⁻¹) |  
| b-hive: a modular training framework for state-of-the-art object-tagging within the Python ecosystem at the CMS experiment | **CMS-DP-2024/020** | [CDS](https://cds.cern.ch/record/2896100) | [twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsDP24020) | | Simulation only |
| Run 3 commissioning results of heavy-flavor jet tagging at √s=13.6 TeV with CMS data using a modern framework for data processing | **DP-2024-024**  |[CDS](https://cds.cern.ch/record/2898463) | [wiki](DPNotesRun3/DP-2024-024/DP-2024-024.md) | [plots](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun3/DP-2024-024/plots) |Run 3 2022+2023 (61.7fb⁻¹) data + simulation|  
| Performance summary of AK4 jet b tagging with data from 2022 proton-proton collisions at 13.6 TeV with the CMS detector | **CMS-DP-2024/025** | [CDS](https://cds.cern.ch/record/2898464) |  |   | Run 3 2022 |
| A unified approach for jet tagging in Run 3 at √s=13.6 TeV in CMS | **CMS-DP-2024/066** | [CDS](https://cds.cern.ch/record/2904702) |  |   | Run 3 2023 Simulation only |
| Jet tagging performance in Run 3 PbPb collisions at 5.36 TeV in CMS | **CMS-DP-2024/088** | [CDS](https://cds.cern.ch/record/2915245) | [wiki](DPNotesRun3/DP-2024-088/DP-2024-088.md) | [plots](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun3/DP-2024-088/plots) | Run 3 2023 PbPb Simulation only |







## Detector Performance Notes at Run 2

#### Latest Developments in 2022

| **Topic** | **DP number** | **CDS entry** | **Approved plots** | **Remarks** |
| ------ | ------ | ------ | ------ | ------ | 
| Efficiency parameterization of b-tagging classifier using Graph Neural Networks | CMS-DP-2022/051 | [link](https://cds.cern.ch/record/2839921) | [plots](https://cds.cern.ch/record/2839921) | Simulation only |
| Transformer models for heavy flavor jet identification | CMS-DP-2022/050 | [link](https://cds.cern.ch/record/2839920) |  | Simulation only |
| Adversarial training for b-tagging algorithms in CMS | CMS-DP-2022/049 | [link](https://cds.cern.ch/record/2839919) |  | Legacy 2017 data |
| Expected Performance of Run-3 HLT b-quark jet identification | CMS-DP-2022/030 | [link](https://cds.cern.ch/record/2825704) | [plots](https://cds.cern.ch/record/2839919) | Run3 Simulation only |

<!-- Older Detector Performance Notes and Physics Analysis Summaries are available in this [[TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsBTV)]. -->


### DP notes for full Run 2 data

| **Topic** | **DP number** | **CDS entry** | **Wiki page** | **Approved plots** | **Remarks** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| AK4CHS jets b-tagging SFs | **DP-2023-005** | [link](https://cds.cern.ch/record/2854609) | [link](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun2/DP-2023-005/DP-2023-005.md) | [link](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun2/DP-2023-005/plots) | Run2 *Ultra Legacy* (138fb⁻¹) |
| AK4CHS jets c-tagging SFs | **DP-2023-006** | [link](https://cds.cern.ch/record/2854610) | [link](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun2/DP-2023-006/DP-2023-006.md) | [link](https://gitlab.cern.ch/cms-btv-public/btv-public-information/-/tree/master/docs/DPNotesRun2/DP-2023-006/plots) | Run2 *Ultra Legacy* (138fb⁻¹) |
| Performance of the mass-decorrelated DeepDoubleX classifier for double-b and double-c large-radius jets with the CMS detector | CMS-DP-2022/041 | [link](https://cds.cern.ch/record/2839736) |  |  |
| Calibration of the mass-decorrelated ParticleNet tagger for boosted bb̅ and cc̅ jets using LHC Run 2 data | CMS DP-2022/005 | [link](https://cds.cern.ch/record/2805611) | | [plots](https://cds.cern.ch/record/2805611) | Legacy Run2 data |




### Physics Analysis Summary and Performance Notes at 13 TeV Collision Data in Year 2018
| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** | **Remark** |
| --- | --- | --- | --- | --- |
| --- | --- | --- | --- | --- |
| B-tagging performance of the CMS Legacy dataset 2018 | CMS DP-2021/004 | [link](https://cds.cern.ch/record/2759970) | [plots](https://twiki.cern.ch/twiki/bin/view/CMSPublic/BTV13TeVUL2018) | 2018 Legacy vs. EOY performance |
| Performance of b tagging algorithms in proton-proton collisions at 13 TeV with Phase 1 CMS detector | CMS DP-2018/033 | [link](https://cds.cern.ch/record/2627468) | [plots](https://twiki.cern.ch/twiki/bin/view/CMSPublic/BTV13TeV2017FIRST2018) | Early 2018 data + 2017 data |
| Performance of Deep Tagging Algorithms for Boosted Double Quark Jet Topology in Proton-Proton Collisions at 13 TeV with the Phase-0 CMS Detector | CMS DP-2018/046 | [link](http://cds.cern.ch/record/2630438) | [plots](https://twiki.cern.ch/twiki/bin/view/CMSPublic/BTV13TeVDDBDDC) | Phase 0 detector, simulation only |


### Physics Analysis Summary and Performance Notes at 13 TeV Collision Data in Year 2017
| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** | **Remark** |
| --- | --- | --- | --- | --- |
| Efficiency parameterization of b-tagging classifier using Graph Neural Networks | CMS-DP-2022/051 | [link](https://cds.cern.ch/record/2771945) | [plots](https://cds.cern.ch/record/2771945) | Simulation only |
| Transformer models for heavy flavor jet identification | CMS-DP-2022/050 | [link](https://cds.cern.ch/record/2771945) |  | Simulation only |
| Adversarial training for b-tagging algorithms in CMS | CMS-DP-2022/049 | [link](https://cds.cern.ch/record/2771945) |  | Legacy 2017 data |
| Expected Performance of Run-3 HLT b-quark jet identification | CMS-DP-2022/030 | [link](https://cds.cern.ch/record/2771945) | [plots](https://cds.cern.ch/record/2771945) | Run3 Simulation only |

### Physics Analysis Summary and Performance Notes at 13 TeV Collision Data in Year 2016
| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** | **Additional Plots** |
| --- | --- | --- | --- | --- |
| b-jet Identification in the CMS experiment (2011 data) | [CMS PAS BTV-11-004](http://cms.cern.ch/iCMS/analysisadmin/analysismanagement?awg=BTV&awgyear=2011&dataset=All) | [link](http://cdsweb.cern.ch/record/1427247) | [plots](http://cdsweb.cern.ch/record/1427247) |     | (superseded by [Paper](http://cdsweb.cern.ch/search&cc=CMS&as=1&sc=1&m1=a&p1=BTV&f1=reportnumber&op1=a&m2=a&p2=&f2=&op2=a&m3=a&p3=&f3=&action_search=Search&dt=&d1d=&d1m=&d1y=&d2d=&d2m=&d2y=&sf=&so=a&sf=&rg=10&sc=1&of=hb&c=CMS+Papers)) |
| Measurement of the b-tagging efficiency using $t\bar t$ events (2011 data) | [CMS PAS BTV-11-003](http://cms.cern.ch/iCMS/analysisadmin/analysismanagement?awg=BTV&awgyear=2011&dataset=All) | [link](http://cdsweb.cern.ch/record/1421611) | [plots](http://cdsweb.cern.ch/record/1421611) | [plots for diff techniques](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PasBTV11003Appendix) | (superseded by [Paper](http://cdsweb.cern.ch/search&cc=CMS&as=1&sc=1&m1=a&p1=BTV&f1=reportnumber&op1=a&m2=a&p2=&f2=&op2=a&m3=a&p3=&f3=&action_search=Search&dt=&d1d=&d1m=&d1y=&d2d=&d2m=&d2y=&sf=&so=a&sf=&rg=10&sc=1&of=hb&c=CMS+Papers)) |
| Commissioning of b-jet identification with pp collisions (2010 data) | [CMS PAS BTV-10-001](http://cms.cern.ch/iCMS/analysisadmin/analysismanagement?awg=BTV&awgyear=2010&dataset=All) | [link](http://cdsweb.cern.ch/record/1279144) | [plots](http://cdsweb.cern.ch/record/1279144) |     |






### Physics Analysis Summary and Performance Notes at 13 TeV collision data in year 2015
| **Topic** | **CADI Entry (Restricted)** | **CDS link** | **Approved plots** | **Remark** |
| --- | --- | --- | --- | --- |
| Identification of b jets using boosted decision trees | CMS PASBTV-16-001 | [link](https://cds.cern.ch/record/2131942) | [plots](https://cds.cern.ch/record/2131942) | 25ns 2015 Reprocessed 76X data (Moriond) |
| Identification of double-b quark jets in boosted event topologies | CMS PASBTV-15-002 | [link](https://cds.cern.ch/record/2061943) | [plots](https://cds.cern.ch/record/2061943) | 25ns 2015 Reprocessed 76X data (Moriond) |
| Identification of b quark jets at the CMS Experiment in the LHC Run 2 | CMS PASBTV-15-001 | [link](https://cds.cern.ch/record/2061944) | [plots](https://cds.cern.ch/record/2061944) | 25ns 2015 Reprocessed 76X data (Moriond) |
| Performance of b-tagging algorithms in 13TeVpp collisions with a bunch spacing of 25ns | CMS DP-2015/056 | [link](https://cds.cern.ch/record/2111945) | [plots](https://cds.cern.ch/record/2111945) | 25ns 2015 data (December Jamboree) |
| Performance of b-tagging algorithms in 13TeVpp collisions with a bunch spacing of 50ns | CMS DP-2015/045 | [link](https://cds.cern.ch/record/2051946) | [plots](https://cds.cern.ch/record/2051946) | 50ns data |
| b-tagging in boosted topology events at 13TeV | CMS DP-2015/038 | [link](https://cds.cern.ch/record/2021947) | [plots](https://cds.cern.ch/record/2021947) | simulation only (superseded byBTV-15-002) |
| b-tagging at HLT |  |  | [plots](https://cds.cern.ch/record/2001948) | look for 'HLT b-tagging Performance for CHEP2015' |





## Detector Performance Notes at Run 1

### Physics Analysis Summary and Performance Note at 8 TeV collision data in year 2012

| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** | **Additional Plots** |
| --- | --- | --- | --- | --- |
| Performance of b tagging in boosted topology events | CMS DP-2014/031 | [iCMS link](https://cds.cern.ch/record/2111950) | [plots](https://cds.cern.ch/record/2111950) |  | (note: part on H->bb superseded byBTV-15-002) |
| Performance of b tagging atTeVin multijet,and boosted topology events | CMS PASBTV-13-001 | [link](https://cds.cern.ch/record/2131951) | [plots](https://cds.cern.ch/record/2131951) | [plots](https://cds.cern.ch/record/2131951) |  |
| Results on b-tagging identification in 8TeVpp collisions (Moriond 2013) | CMS DP-2013/005 | [link](https://cds.cern.ch/record/2041952) | [plots](https://cds.cern.ch/record/2041952) |  | (superseded by BTV-13-001) |

### Physics Analysis Summaries at 7 TeV collision data in years 

| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** | **Additional Plots** |
| --- | --- | --- | --- | --- |
| b-jet Identification in the CMS experiment (2011 data) | CMS PASBTV-11-004 | [link](https://cds.cern.ch/record/1381953) | [plots](https://cds.cern.ch/record/1381953) |  | (superseded byPaper) |
| Measurement of the b-tagging efficiency usingevents (2011 data) | CMS PASBTV-11-003 | [link](https://cds.cern.ch/record/1361954) | [plots](https://cds.cern.ch/record/1361954) | [plots](https://cds.cern.ch/record/1361954) | (superseded byPaper) |
| Commissioning of b-jet identification with pp collisions (2010 data) | CMS PASBTV-10-001 | [link](https://cds.cern.ch/record/1351955) | [plots](https://cds.cern.ch/record/1279144)
| Distributions of B-tag observables and event display from 7 TeV data | DP-2010/015 | [link](http://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20DP-2010/015)  |  [plots](http://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20DP-2010/015)  |
| Event display from the 2009 run with a reconstructed secondary vertex | DP-2010/003 | [link](http://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20DP-2010/003)  |  [plots](http://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20DP-2010/003)  |

### Older Physics Analysis Summaries
| **Topic** | **CADI Entry (restricted)** | **CDS Entry** | **Approved Plots** |
| --- | --- | --- | --- |
| Description and performance of CMS track & PV reconstruction | [TRK-10-001](https://cms.cern.ch/iCMS/analysisadmin/cadilines?line=TRK-11-001&tp=an&id=611&ancode=TRK-11-001) | [link](CERN-PH-EP-2014-070) | [link](CERN-PH-EP-2014-070)| 
| MC studies |  |  [link](https://cds.cern.ch/search?cc=CMS&ln=en&p=reportnumber%3ABTV+AND+NOT+6531_a%3AData&f=&action_search=Search&c=CMS+Physics+Analysis+Summaries&c=&sf=&so=d&rm=&rg=10&sc=1&of=hb) | [link](https://cds.cern.ch/search?cc=CMS&ln=en&p=reportnumber%3ABTV+AND+NOT+6531_a%3AData&f=&action_search=Search&c=CMS+Physics+Analysis+Summaries&c=&sf=&so=d&rm=&rg=10&sc=1&of=hb)  |
